package com.semenov.userservice.repository;

import com.semenov.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findUserByIdentityNumber(String identityNumber);
}

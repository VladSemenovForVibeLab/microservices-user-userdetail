package com.semenov.userservice.service;

import com.semenov.userservice.client.UserDetailClient;
import com.semenov.userservice.model.User;
import com.semenov.userservice.model.UserDetail;
import com.semenov.userservice.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserDetailClient userDetailClient;

    public UserService(UserRepository userRepository, UserDetailClient userDetailClient) {
        this.userRepository = userRepository;
        this.userDetailClient = userDetailClient;
    }

    public User addUser(User user){
        return userRepository.save(user);
    }

    public User getUserAndDetail(String identityNumber){
        UserDetail userDetail = userDetailClient.getUserDetailByIdentityNumber(identityNumber).getBody();
        User user = userRepository.findUserByIdentityNumber(identityNumber).orElse(null);
        assert user != null;
        user.setUserDetail(userDetail);
        return user;
    }
}
